import 'package:codezero_application/model/post.dart';
import 'package:flutter/material.dart';

class PostCard extends StatelessWidget {
  const PostCard({Key? key, required this.post}) : super(key: key);

  final Post post;

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Colors.red,
      child: SizedBox(
        height: (MediaQuery.of(context).size.width / 2 - 20) * 3.1,
        width: MediaQuery.of(context).size.width / 2 - 20,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 15,
              ),
              child: Row(
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.fill, image: NetworkImage(post.icon))),
                  ),
                  const SizedBox(width: 10),
                  Text(
                    post.name,
                    overflow: TextOverflow.clip,
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: (MediaQuery.of(context).size.width / 2 - 20) * 1.8,
              child: Image.network(
                post.backgroundUrl,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 10.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.fill, image: NetworkImage(post.icon))),
                  ),
                  const SizedBox(width: 10),
                  Flexible(
                    child: Text(
                      '${post.name}\n${post.description}\n17 December 2019',
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.start,
                      style: const TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Flexible(
                child: Text(
                  post.body,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.start,
                  maxLines: 4,
                  style: TextStyle(fontSize: 12, color: Colors.black),
                ),
              ),
            ),
            const Spacer(),
            Container(
              color: Colors.black,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _buildBottomICons(Icons.star_outline, post.likeCount),
                  _buildBottomICons(Icons.share, post.shareCount),
                  _buildBottomICons(
                      Icons.remove_red_eye_outlined, post.viewCount,
                      isReversed: true)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBottomICons(IconData icon, int value,
      {bool isReversed = false}) {
    List<Widget> widgets = [
      Icon(
        icon,
        color: Colors.grey,
      ),
      const SizedBox(width: 5),
      Text(
        value.toString(),
        style: TextStyle(color: Colors.grey),
      )
    ];

    if (isReversed) widgets.reversed;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: widgets,
    );
  }
}
