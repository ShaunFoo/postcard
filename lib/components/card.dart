import 'package:flutter/material.dart';

class Card extends StatelessWidget {
  const Card({
    Key? key,
    required this.child,
    this.height,
    this.width,
    this.borderRadius,
    this.boxShadowColor,
    this.boxShadowOffset = const Offset(1, 2.5),
    this.boxShadowBlurRadius = 5,
    this.boxShadowSpreadRadius = 1,
  }) : super(key: key);

  final double? height;
  final double? width;
  final BorderRadius? borderRadius;
  final Color? boxShadowColor;
  final Offset boxShadowOffset;
  final double boxShadowBlurRadius;
  final double boxShadowSpreadRadius;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    Color _boxShadowColor = boxShadowColor != null
        ? boxShadowColor!
        : const Color.fromRGBO(60, 126, 232, 1).withOpacity(0.2);
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius:
            borderRadius ?? const BorderRadius.all(Radius.circular(7)),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: _boxShadowColor,
            offset: boxShadowOffset,
            blurRadius: boxShadowBlurRadius,
            spreadRadius: boxShadowSpreadRadius,
          )
        ],
      ),
      child: child,
    );
  }
}
