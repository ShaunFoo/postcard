import 'dart:convert';

import 'package:codezero_application/components/post_card.dart';
import 'package:codezero_application/model/post.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CodeZero Application',
      theme: ThemeData.dark(),
      home: const MyHomePage(title: 'My PostCards'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Center(
          child: FutureBuilder(
            future: getPosts(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (!snapshot.hasData) {
                return SizedBox();
              }
              List<Widget> widgets = [];

              snapshot.data!.forEach((element) {
                widgets.add(PostCard(post: element));
              });
              var size = MediaQuery.of(context).size;

              final double itemHeight =
                  (size.height - kToolbarHeight - 24) * 0.9;
              final double itemWidth = size.width / 2;

              return GridView.count(
                  primary: false,
                  childAspectRatio: (itemWidth / itemHeight),
                  padding: const EdgeInsets.only(
                      top: 5, bottom: 30, left: 5, right: 5),
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 5,
                  crossAxisCount: 2,
                  children: widgets);
            },
          ),
        ),
      ),
    );
  }

  Future<List> getPosts() async {
    var url = Uri.parse('https://dev.codezero.app/api/v2/posts');
    var response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Authorization':
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZjOGM3Mzk3YTA5Mzc3YWE4ZDI0YjVlZDZmNGU1MmViMGE4MmRjYTA1N2RmNTViZjFhOWVmY2Q2ZjczMmJkZGIwNTk4MzBmMzY1OTdmNWZmIn0.eyJhdWQiOiIyIiwianRpIjoiNmM4YzczOTdhMDkzNzdhYThkMjRiNWVkNmY0ZTUyZWIwYTgyZGNhMDU3ZGY1NWJmMWE5ZWZjZDZmNzMyYmRkYjA1OTgzMGYzNjU5N2Y1ZmYiLCJpYXQiOjE2NDUzNjc5OTEsIm5iZiI6MTY0NTM2Nzk5MSwiZXhwIjoxNjc2OTAzOTkxLCJzdWIiOiI5OSIsInNjb3BlcyI6WyIqIl19.DreN8FMxLy-YzPaaO3KBsyMZeqs0vkhAuz2OKpEUgKJGLWon8Ov0EnHLavl_huRCtPWd-gMXZLMM78Dw8o-qn-VuQRmKG4cZMdBx5DprokpnXvidHcpSeixNwPeHZLhQdo1RxjkyTNErL9NukPX45899CASS-yTmSY7CnB2OOOjgf2ppqreCSiLaw-TX3RBLrFvD8NOjEUJk8alen-Kp9fb6cGPABEP1VB9d4y5H5mD_KuOYdBprw9ZsABqMGPv552ARpfMHL-fKHTytTDkSS5LyunQhb_LHdvqQnlXhf5Cbu_9FVUlVCXcZe_Rf6XRSB9x6KW4qOqW-9yftYQ5qF4cEK3iDkeaxf_8spo20eRmxJwx1LW4pFxMNdhtdj5aNyIBuGMO8uzpr0lJ0Pr0vcvxtoaZlcq9Y5PigpSho6KM5zV1e2PEirKAX11EYJlzx_x56P14bAwRyU-4EGHxWayAt4dy8NGFKueUeEvgfdrUXokhz0A7J5Dy18U7L6OwBvPBSKnX60KW1EOAa7wPOliXrH3eKO-qPgnEuC33GbByuITfDNEn-pDeExO0_ENuxRgFNXl70ImBCulTz84l61dXT4KQ5wBsXaQaQG-7n2OuFQef5cdxWsENiGRPdjYQ3M5V-Z-b7eHX7JHVXxAeXXwBNlLS02Ie2G4sQji95y8s',
    });
    Map json = jsonDecode(response.body);

    await Future.delayed(const Duration(seconds: 2), () {});
    List<dynamic> posts =
        json["data"].map((dynamic post) => Post.fromJson(post)).toList();

    return posts;
  }
}
