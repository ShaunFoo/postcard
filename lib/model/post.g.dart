// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) => Post(
      json['id'] as int,
      json['card_id'] as int,
      json['user_id'] as int,
      json['timestamp'] as String,
      json['type'] as String,
      json['name'] as String,
      json['icon'] as String,
      json['description'] as String,
      json['background_url'] as String,
      json['co_name'] as String?,
      json['co_icon'] as String?,
      json['liked'] as bool,
      json['like_count'] as int,
      json['viewed'] as bool,
      json['view_count'] as int,
      json['shared'] as bool,
      json['share_count'] as int,
      json['body'] as String,
    );

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'id': instance.id,
      'card_id': instance.cardId,
      'user_id': instance.userId,
      'timestamp': instance.timestamp,
      'type': instance.type,
      'name': instance.name,
      'icon': instance.icon,
      'description': instance.description,
      'background_url': instance.backgroundUrl,
      'co_name': instance.coName,
      'co_icon': instance.coIcon,
      'liked': instance.liked,
      'like_count': instance.likeCount,
      'viewed': instance.viewed,
      'view_count': instance.viewCount,
      'shared': instance.shared,
      'share_count': instance.shareCount,
      'body': instance.body,
    };
