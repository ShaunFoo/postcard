import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@JsonSerializable()
class Post {
  int id;
  @JsonKey(name: 'card_id')
  int cardId;
  @JsonKey(name: 'user_id')
  int userId;
  String timestamp;
  String type;
  String name;
  String icon;
  String description;
  @JsonKey(name: 'background_url')
  String backgroundUrl;
  @JsonKey(name: 'co_name')
  String? coName;
  @JsonKey(name: 'co_icon')
  String? coIcon;
  bool liked;
  @JsonKey(name: 'like_count')
  int likeCount;
  bool viewed;
  @JsonKey(name: 'view_count')
  int viewCount;
  bool shared;
  @JsonKey(name: 'share_count')
  int shareCount;
  String body;

  Post(
      this.id,
      this.cardId,
      this.userId,
      this.timestamp,
      this.type,
      this.name,
      this.icon,
      this.description,
      this.backgroundUrl,
      this.coName,
      this.coIcon,
      this.liked,
      this.likeCount,
      this.viewed,
      this.viewCount,
      this.shared,
      this.shareCount,
      this.body);

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);
  Map<String, dynamic> toJson() => _$PostToJson(this);
}
